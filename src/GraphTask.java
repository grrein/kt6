import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 * @author Greta Reino
 */
public class GraphTask {

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      Graph g = new Graph("G");
      g.createRandomSimpleGraph(2024, 21);

      System.out.println(g);
      System.out.println(g.vertices.get(0) + "  <- input");
      System.out.println(g.longestPointFrom(g.vertices.get(0)) + "  <- farthest from " + g.vertices.get(0));
   }


   /**
    * Vertex is the fundamental unit of which graphs are formed.
    */
   class Vertex {

      private final String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private final String id;
      private Vertex from;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }

   class Graph {

      private final String id;
      private Vertex first;
      private int info = 0;
      private List<Vertex> vertices = new ArrayList<>();
      private final List<Arc> arcs = new ArrayList<>();

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" [");
               sb.append(a.info);
               sb.append("] ");
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }
      /**
       * Method for creating a vertex in given graph.
       */
      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         vertices.add(0, res);
         return res;
      }
      /**
       * Method for creating an arc in given graph.
       */
      public Arc createArc(String aid, Vertex from, Vertex to, int info) {
         if (info == 0) {
            throw new RuntimeException("Arc length cannot be 0");
         }
         Arc res = new Arc(aid);
         res.info = info; //length in this case
         res.next = from.first;
         res.from = from;
         from.first = res;
         res.target = to;
         arcs.add(res);
         return res;
      }

      /**
       * Method to get all vertices after creating a graph.
       * Adds all vertices to an ArrayList defined in graph class.
       */
      public void initializeVerticesList() {
         List<Vertex> vertices = new ArrayList<>();
         Vertex vertex = this.first;
         while (vertex != null) {
            vertices.add(vertex);
            vertex = vertex.next;
         }
         this.vertices = vertices;
      }

      /**
       * Method to find the distance between two vertices (aka arc length) from two vertex ID strings.
       *
       * @param start - Start vertex ID
       * @param end   -  End vertex ID
       * @return arc length (integer)
       */
      public int findArcDistance(String start, String end) {
         for (Arc arc : arcs) {
            if (arc.from.id.equals(start) && arc.target.id.equals(end)) {
               return arc.info;
            }
         }
         return 0; //return 0 if there isn't an arc between two vertices
      }

      /**
       * Create a matrix of arcs, used in getDistMatrix method to find corresponding arc lengths.
       *
       * @return Matrix of arcs.
       */
      public String[][] arcMatrix() {
         int[][] adjMatrix = createAdjMatrix();
         int nvert = adjMatrix.length;
         String[][] arcMatrix = new String[nvert][nvert];
         for (int i = 0; i < nvert; i++) {
            for (int j = 0; j < nvert; j++) {
               arcMatrix[i][j] = vertices.get(i).id + "-" + vertices.get(j).id;
            }
         }
         return arcMatrix;
      }

      /**
       * Method to find a matrix of all the distances in graph. If there is no connection between two vertices,
       * the distance is defined as infinity (in this case Integer.MAX_VALUE divided by 4, should be big enough).
       *
       * @return Matrix of all the distances.
       */
      public int[][] getDistMatrix() {
         int[][] adjMatrix = createAdjMatrix();
         int nvert = adjMatrix.length;
         int[][] distMatrix = new int[nvert][nvert];
         String[][] arcMatrix = arcMatrix();
         if (nvert < 1) return distMatrix;
         int inf = Integer.MAX_VALUE / 4;
         for (int i = 0; i < nvert; i++) {
            for (int j = 0; j < nvert; j++) {
               distMatrix[i][j] = distMatrix[i][j] * -1;
               if (adjMatrix[i][j] == 0) {
                  distMatrix[i][j] = inf;
               } else {
                  int distance = findArcDistance(arcMatrix[i][j].split("-")[0], arcMatrix[i][j].split("-")[1]);
                  distMatrix[i][j] = distance;
               }
            }
         }
         for (int i = 0; i < nvert; i++) {
            distMatrix[i][i] = 0;
         }
         return distMatrix;
      }


      /**
       * Method to find longest paths in given graph.
       *
       * @return Matrix of integers where each integer shows the longest path.
       */
      public int[][] longestPaths() {
         initializeVerticesList();
         int[][] distMatrix = getDistMatrix();
         int n = vertices.size();// number of vertices
         if (n < 1) {
            throw new RuntimeException("Cannot find longest path in an empty graph.");
         }
         for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
               for (int j = 0; j < n; j++) {
                  int newLength = distMatrix[i][k] + distMatrix[k][j];
                  if (distMatrix[i][j] > newLength) {
                     distMatrix[i][j] = newLength; // new path is longer
                  }
               }
            }
         }
         return distMatrix;
      }

      /**
       * Method to find the farthest point from a given vertex in given graph.
       * This is the main method that is used to solve the given task.
       *
       * @param vertex - Vertex to find the farthest point from.
       * @return - Vertex that is the farthest from given vertex.
       */
      public Vertex longestPointFrom(Vertex vertex) {
         int index = vertices.indexOf(vertex);
         int[][] distMatrix = longestPaths();
         int inf = Integer.MAX_VALUE / 4;
         int maxIntIndex = 0;
         int max = 0;
         for (int i = 0; i < distMatrix[index].length; i++) {
            if (distMatrix[index][i] != inf && distMatrix[index][i] > max) {
               max = distMatrix[index][i];
               maxIntIndex = i;
            }
         }
         return vertices.get(maxIntIndex);
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + (n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               int rndLength = (int) (Math.random() * 10 + 1);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i], rndLength);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr], rndLength);
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            int rndLength = (int) (Math.random() * 10 + 1);
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, rndLength);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi, rndLength);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
            initializeVerticesList();
         }
      }
   }
}
